from stringcolor.ops import (
    Bold as bold,
    Color as cs,
    Underline as underline,
)

name = "stringcolor"
